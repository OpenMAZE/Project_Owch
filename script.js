var video = document.getElementById("video");
var playButton = document.getElementById("play-pause");
var muteButton = document.getElementById("mute");
var fullScreenButton = document.getElementById("full-screen");
var seekBar = document.getElementById("seek-bar");
var volumeBar = document.getElementById("volume-bar");
var errtext = document.getElementById("errtext");
var voldiv = document.getElementById("volumediv");
//비디오 재생/정지
playButton.addEventListener("click", function() {
  if (video.paused == true) {
    video.play();
    playButton.innerHTML = "&#xE034;";
  } else {
    video.pause();
    playButton.innerHTML = "&#xE037;";
  }
});
//Mute
muteButton.addEventListener("click", function() {
  if (video.muted == false) {
    video.muted = true;
    muteButton.innerHTML = "&#xE04F;";
    volumeBar.value = 0;
  } else {
    video.muted = false;
    muteButton.innerHTML = "&#xE050;";
    volumeBar.value = 100;
  }
});
//전체화면
fullScreenButton.addEventListener("click", function() {
  if (video.requestFullscreen) {
    video.requestFullscreen();
    errtext.innerHTML = "전체화면이 되지 않나요? <a href='https://goo.gl/forms/qMNQBoNPufdfSD8B2'>신고하기</a>";
  } else if (video.mozRequestFullScreen) {
    video.mozRequestFullScreen(); //파이어폭스
  } else if (video.webkitRequestFullscreen) {
    video.webkitRequestFullscreen(); //크롬&사파리
  } else if (video.msRequestFullscreen) {
    video.msRequestFullscreen();
  } else {
      video.requestFullscreen();
      errtext.innerHTML = "전체화면이 되지 않나요? <a href='https://goo.gl/forms/qMNQBoNPufdfSD8B2'>신고하기</a>";
  }
});
//비디오 시간조정
seekBar.addEventListener("change", function() {
  var time = video.duration * (seekBar.value / 100);
  video.currentTime = time;
});
//비디오 시간 업데이트
video.addEventListener("timeupdate", function() {
  var value = (100 / video.duration) * video.currentTime;
  seekBar.value = value;
  if (seekBar.value >= 99){
    playButton.innerHTML = "&#xE037;";
  }
});
//불륨조절
volumeBar.addEventListener("change", function() {
  video.volume = volumeBar.value;
});

seekBar.addEventListener("mousedown", function() {
        video.pause;
});

seekBar.addEventListener("mouseup", function() {
        video.play;
});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}